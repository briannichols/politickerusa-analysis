## compare.py
##
##  compare absolute word frequencies in two texts
##  Source is http://ling571.wordpress.com/2011/11/09/midterm-program/

from __future__ import division
import nltk
from nltk import FreqDist
from math import log
from nltk.corpus.reader.plaintext import PlaintextCorpusReader
from nltk.corpus import stopwords


def makeKey(freqA, freqB):
    """generate a key for sorting scores -- sort by score first, then by
       frequency,then by alphabetical order
    """
    def key(pair):
        word, score = pair
        freq = freqA[word] + freqB[word]

        if score < 0.0:
            ## if score is negative, the higher frequencies should come earlier
            freq = -freq

        return (score, freq, word)

    return key


def coeff(x, y, s):
    """coefficient of difference for two absolute frequencies"""
    y *= s
    return (x - y) / (x + y)


def differences(freqA, freqB, cutoff=10, s=1.0):
    """calculate coefficients of difference from two frequency dictionaries"""
    freqBoth = freqA + freqB
    diff = dict()

    for word in freqBoth:
        if freqBoth[word] > cutoff:
            diff[word] = coeff(freqA[word], freqB[word], s)
            # *log(freqBoth[word])

    return diff


def norm(words):
    """process a corpus into a normalized list of words"""
    stopwords = nltk.corpus.stopwords.words('english')
    return [w.lower() for w in words if w.isalpha()
                                     and w not in stopwords
                                     and len(w) > 2]


### MAIN
### def main():
c = PlaintextCorpusReader('data/', ['democrat.txt', 'republican.txt'])
a = FreqDist(norm(c.words('democrat.txt')))
b = FreqDist(norm(c.words('republican.txt')))
s = len(c.words('democrat.txt')) / len(c.words('republican.txt'))

diff = differences(a, b, 500, s)

width = max(len(w) for w in diff)

print '%*s %5s %5s %7s' % (-width, 'WORD', 'DEM', 'REP', 'CD')
print

for word, score in sorted(diff.items(), key=makeKey(a, b)):
    print '%*s %5d %5d %7.4f' % (-width, word, a[word], b[word], score)

###main()
